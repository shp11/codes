from django.shortcuts import render_to_response
from django.contrib.auth import authenticate,login
from django.contrib.auth.models import User
from django import forms
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from create.models import loginInfo, profileDetails
from create.serializers import loginInfoSerializer, profileDetailsSerializer
from create.forms import loginForm, createProfile

def index(request):
  if request.method == 'GET':
    form = loginForm()
    return render_to_response('index.html', {'form':form} , context_instance=RequestContext(request))

  elif request.method == 'POST':
    form = loginForm(request.POST)
    if form.is_valid():
      username = request.POST['username']
      password = request.POST['password']
    else:
      message = 'Form has errors'
      return render_to_response('index.html', {'form':form,'message':message} , context_instance=RequestContext(request))
  user = loginInfo.objects.get(username = username)

  if user.password == password:
    request.session['user_id'] = user.id
    return HttpResponseRedirect('/create/choice')
  else:
    message = 'Username/Password Incorrect'
    return render_to_response('index.html', {'form':form,'message':message} , context_instance=RequestContext(request))

def create_profile(request):
  form = createProfile
  return render_to_response('create_profile.html', {'form':form}, context_instance = RequestContext(request))

def choice(request):
  userId = request.session['user_id']
  return render_to_response('choice.html',{'userId':userId}, context_instance = RequestContext(request))

@api_view(['POST'])
def save_details(request):
  form = createProfile(request.POST)
  if request.method == 'POST':
    if 'user_id' in request.session:
      userId = int(request.session['user_id'])
    else:
      return HttpResponse("You are not logged in")
    if form.is_valid():
      name = form.cleaned_data['name']
      age = form.cleaned_data['age']
      sex = form.cleaned_data['sex']
      location = form.cleaned_data['location']
      phone = form.cleaned_data['phone']
      email = form.cleaned_data['email']
      profile = profileDetails(userId, name, age, sex, location, phone, email)
      profile.save(force_insert = True)
    else:
      message = 'Form has errors'
      return render_to_response('create_profile.html', {'form':form,'message':message} , context_instance=RequestContext(request))
  
    return HttpResponse("Your profile has been created")
  
@api_view(['GET','PUT','DELETE'])
def profile_operations(request, pk = 0):
  form = createProfile(request.POST)
  pkey = int(pk)
  if 'user_id' in request.session:
    userId = int(request.session['user_id'])
  else:
    return HttpResponse("You are not logged in")
  if pkey != userId:
    return HttpResponse("Not a valid userId")
  try:
    profile = profileDetails.objects.get(pk=pk)
  except profileDetails.DoesNotExist:
    message = 'No profile created'
    return render_to_response('create_profile.html', {'form':form,'message':message} , context_instance=RequestContext(request))
  
  if request.method == 'GET':
    serializer = profileDetailsSerializer(profile)
    return Response(serializer.data)
  
  elif request.method == 'PUT':
    serializer = profileDetailsSerializer(profile,data = request.DATA)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    else:
      return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

  elif request.method == 'DELETE':
    profile.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)