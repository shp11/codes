from django import forms
from create.models import loginInfo,profileDetails

class loginForm(forms.Form):
  username = forms.CharField(max_length=100)
  password = forms.CharField(widget=forms.PasswordInput())
  
  class Meta:
    model = loginInfo
    fields = ('username','password')
      
class createProfile(forms.Form):
  name = forms.CharField(max_length=100)
  age = forms.IntegerField()
  sex_choices = (
      ('Male','Male'),
      ('Female','Female'),
    )
  sex = forms.ChoiceField(choices=sex_choices,label = 'Sex')
  location = forms.CharField(max_length=100)
  phone = forms.IntegerField()
  email = forms.EmailField()
  
  class Meta:
    model = profileDetails
    fields = ('name','age','sex','location','phone','email')