from django.conf.urls import patterns, url
from create.views import index,create_profile,save_details,choice,profile_operations

urlpatterns = patterns('',
    #'create.views',
    url(r'^login',index),
    url(r'^create',create_profile),
    url(r'^save',save_details),
    url(r'^choice',choice),
    url(r'^options/(?P<pk>[0-9]+)',profile_operations),
)