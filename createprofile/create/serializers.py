from rest_framework import serializers

from create.models import loginInfo, profileDetails

class loginInfoSerializer(serializers.ModelSerializer):
  class Meta:
    model = loginInfo
    fields = ('username','password')
    
class profileDetailsSerializer(serializers.ModelSerializer):
  class Meta:
    model = profileDetails
    fields = ('userId','name','age','sex','location','phone','email')