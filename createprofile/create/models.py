from django.db import models

# Create your models here.
class loginInfo(models.Model):
  username = models.CharField(max_length=100)
  password = models.CharField(max_length=100)

class profileDetails(models.Model):
  userId = models.ForeignKey(loginInfo,primary_key = True)
  name = models.CharField(max_length=100)
  age = models.IntegerField()
  sex_choices = (
      ('Male','Male'),
      ('Female','Female'),
    )
  sex = models.CharField(max_length=10,choices=sex_choices)
  location = models.CharField(max_length=100)
  phone = models.IntegerField()
  email = models.EmailField()