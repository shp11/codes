# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('create', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='profileDetails',
            fields=[
                ('userId', models.ForeignKey(primary_key=True, to_field=u'id', serialize=False, to='create.loginInfo')),
                ('name', models.CharField(max_length=100)),
                ('age', models.IntegerField()),
                ('sex', models.CharField(max_length=10, choices=[('Male', 'Male'), ('Female', 'Female')])),
                ('location', models.CharField(max_length=100)),
                ('phone', models.IntegerField()),
                ('email', models.EmailField(max_length=75)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
