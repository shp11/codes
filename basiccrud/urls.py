from django.conf.urls import patterns, include, url
from django.contrib import admin
from crud.views import get_all_details, get_details, new_details, delete_details, add_details, update_details, edit_details
from crudss.views import user_list,user_create,user_update,user_delete

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'basiccrud.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^restframe/',include('restframe.urls')),
    url(r'^get/$',get_all_details),
    url(r'^get/(?P<u_id>\d+)/$',get_details),
    url(r'^post/',add_details),
    url(r'^new/',new_details),
    url(r'^edit/(?P<u_id>\d+)/$',edit_details),
    url(r'^put/(?P<u_id>\d+)/$',update_details),
    url(r'^delete/(?P<u_id>\d+)/$',delete_details),
    url(r'^show$',user_list),
    url(r'^new_user$',user_create),
    url(r'^edit_user/(?P<pk>\d+)$',user_update),
    url(r'^delete_user/(?P<pk>\d+)$',user_delete),
)
