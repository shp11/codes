from django.db import models
from django.core.urlresolvers import reverse

class user_details(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    