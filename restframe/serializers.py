from rest_framework import serializers

from restframe.models import user_details

class UserSerializer(serializers.ModelSerializer):

	class Meta:
		model = user_details
		fields = ('name','email')
