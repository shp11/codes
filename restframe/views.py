from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from restframe.models import user_details
from restframe.serializers import UserSerializer

@api_view(['GET','POST'])
def user_list(request):
	if request.method == 'GET':
		users = user_details.objects.all()
		serializer = UserSerializer(users)
		return Response(serializer.data)
	
	elif request.method == 'POST':
		serializer = UserSerializer(data = request.DATA)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT','DELETE'])
def user_detail(request, pk = 0):
	try:
		user = user_details.objects.get(pk=pk)
	except user_details.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
	
	if request.method == 'GET':
		serializer = UserSerializer(user)
		return Response(serializer.data)
	
	elif request.method == 'PUT':
		serializer = UserSerializer(user,data = request.DATA)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		user.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

