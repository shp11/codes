from django.conf.urls import patterns, url

urlpatterns = patterns(
  'restframe.views',
  url(r'^users/$','user_list', name = 'user_list'),
  url(r'^users/(?P<pk>[0-9]+)','user_detail', name = 'user_detail'),    
)
